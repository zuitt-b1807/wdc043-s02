import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        /*
             apple avocado banana kiwi orange

             ask the user which fruit they would like

             the index number of fruit

             //arraylist of string add 4 name or your friends

             output concatenated with a string message in the console

             hashmap of keys
             output contents of the hasmap
             toothpaste=15
        */

        String[] fruits = {"apple","avocado","banana","kiwi","orange"};
        System.out.println("Fruits in stock:" + Arrays.toString(fruits));

        Scanner scannerInput = new Scanner(System.in);

        System.out.println("What fruit would you like to get the index of?");
        String fruitName = scannerInput.nextLine();
        int fruitIndex = Arrays.binarySearch(fruits, fruitName);

        System.out.println("The index of kiwi is: " + fruitIndex);

        ArrayList<String> friends = new ArrayList<>();
        //ArrayList Methods
        //arrayListName.add(<itemToAdd>) - adds elements in our array list.
        friends.add("Telle");
        friends.add("Joseph");
        friends.add("Paz");
        friends.add("Bill");
        System.out.println("My friends are: " + friends);


        HashMap<String,Integer> inventory = new HashMap<>();
        inventory.put("tootpaste", 15);
        inventory.put("tootbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consist of :" + System.lineSeparator() + inventory);

    }
}